package com.pathmazing.tesjor.testmodule.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by Pathmazing on 5/18/2016.
 * <p>
 * Upgrade by Kevin on 26/09/2016
 * <p>
 */
public class Authorize {

    private final SharedPreferences preferences;
    private Context context;


    public Authorize(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(this.getClass().getSimpleName(), Activity.MODE_PRIVATE);
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void parseData(JSONObject data) {
        SharedPreferences.Editor editor = preferences.edit();
        Iterator<String> iter = data.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            try {
                editor.putString(key, data.getString(key));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        editor.apply();
    }

    public String getToken() {
        return preferences.getString("token", "");
    }

    public void setToken(String token) {
        preferences.edit().putString("token", token).apply();
    }

    public String getRefreshToken() {
        return preferences.getString("refresh_token", "");
    }

    public void setRefreshToken(String token) {
        preferences.edit().putString("refresh_token", token).apply();
    }

    public void setAccessToken(String accessToken) {
        if (accessToken == null)
            preferences.edit().remove("AccessToken").apply();
        else
            preferences.edit().putString("AccessToken", accessToken).apply();
    }

    public String getAccessToken() {
        return preferences.getString("AccessToken", "");
    }

    public void clear() {
        preferences.edit().clear().apply();
    }

    public static String getToken(Context context) {
        return new Authorize(context).getToken();
    }

    public static String getRefreshToken(Context context) {
        return new Authorize(context).getRefreshToken();
    }

    public static String getAccessToken(Context context) {
        return new Authorize(context).getAccessToken();
    }

    public static void clear(Context context) {
        new Authorize(context).clear();
    }
}
